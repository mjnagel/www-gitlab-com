---
layout: handbook-page-toc
title: "Learning Marketing Operations"
description: "This handbook page is to provide updated marketing operations learning and resources materials."
---

{::options parse_block_html="true" /}

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
  {:toc .hidden-md .hidden-lg}

# Learning Marketing Operations

At GitLab, we know how important it is to leverage the right Marketing technologies and tools to support marketing campaigns and generate sales opportunities. The tools can only do so much, it's up to the Marketing Operations and team to drive the strategic direction and adoption to maximize the results and impact.

To do so, we need to embrace continuous learning to stay on top of industry trends and best practices. This handbook page updated quarterly will be your main source of resources for all things Marketing Operations.

## What is Marketing Operations?

Defined by Gartner, "Marketing Operations is the function of overseeing an organization's marketing program, campaign planning and annual strategic planning activities. Other responsibilities include technology and performance measurement and reporting and data analytics."

-   [Marketo - The definitive guide to marketing operations (MOps)](https://business.adobe.com/blog/basics/definitive-guide-to-marketing-operations-mops)
-   [Zapier - The ultimate guide to Marketing Operations](https://zapier.com/blog/marketing-operations/)
-   [MarTech - What is Marketing Operations and who are MOps professionals?](https://martech.org/what-is-marketing-operations-and-who-are-mops-professionals/)

## 🔥 Hot Topics

Last updated: July 2023

### Marketing Trends

-   📝 [Business News Daily - 10 tech trends that will influence your Marketing strategies](https://www.businessnewsdaily.com/8564-future-of-marketing.html)
-   📝 [Gartner - What will Marketing focus on in 2023?](https://www.gartner.com/en/articles/what-will-marketing-focus-on-in-2023)

### Personalization

-   🎙️[Human of MarTech - A time traveler's guide to martech and personalization](https://humansofmartech.com/2023/06/06/74-pratik-desai-a-time-travelers-guide-to-martech-and-personalization/)
-   📝[Instapage - How to use Account-Based Marketing personalization in your campaigns](https://instapage.com/blog/account-based-marketing-personalization/)
-   📝 [LinkedIn - Hyper-personalization in ABM: Doing it throughout the funnel](https://www.linkedin.com/pulse/hyper-personalization-abm-doing-throughout-funnel-naseef-kpo/)

### AI

-   📝 [LXA - What is the future of AI in Marketing?](https://www.lxahub.com/stories/what-is-the-future-of-ai-in-marketing)
-   📝 [MarketingOps - Artificial Intelligence Basics a Primer for Marketing Ops Professionals](https://marketingops.com/artificial-intelligence-basics-a-primer-for-marketing-ops-professionals/)
-   📝 [Martech - How to accelerate your Marketing career using generative AI now](https://martech.org/how-to-accelerate-your-marketing-career-using-generative-ai-now/)

### Data

-   🎙️ [The OG Ops - Make pipeline, not war - The attribution](https://open.spotify.com/episode/4eEqTUQvATBu57CNvNxO1K?si=AfkbItDLSKi9uKH4oIhjOA)
-   📝 [Media Village - First-party data, the end of third-party cookies and the future of targeting](https://www.mediavillage.com/article/first-party-data-the-end-of-third-party-cookies-and-the-future-of-targeting/)
-   📝 [Hyperproof - Data compliance: What you need to know](https://hyperproof.io/resource/data-compliance/)
-   🎙️ [CS2 - Opt-in optimized: Understanding data compliance](https://www.cs2marketing.com/fwd-thinking/opt-in-optimized-understanding-data-compliance)

## 🏗️ Marketing Operations Fundamentals

### Marketo

-   [Marketo Engage Tutorials](https://experienceleague.adobe.com/docs/marketo-learn/tutorials/overview.html?lang=en)
-   [Marketo Engage Learning Path](https://learning.adobe.com/learning-path/adobe_marketo_engage/curra000000000006231.html)
-   [Getting Started with Marketo](https://experienceleague.adobe.com/docs/marketo/using/getting-started-with-marketo/getting-started.html)
-   [Adobe Summit 2023](https://business.adobe.com/summit/2023/sessions.html?Products=Adobe+Marketo+Engage)

### Salesforce

-   [Salesforce Trailhead - Learn CRM fundamentals for Salesforce Classic](https://trailhead.salesforce.com/content/learn/trails/getting_started_crm_basics)
-   [LinkedIn Learning - Salesforce Essential Training (Lightning Experience)](https://www.linkedin.com/learning/salesforce-essential-training/accessing-salesforce-through-your-company?autoSkip=true&resume=false&u=2255073)
-   [Salesforce Help Docs - Build a report in Salesforce Classic](https://help.salesforce.com/s/articleView?id=sf.reports_builder_editing.htm&type=5)

### Buyer Journey

-   📝 [Zendesk - What is the buyer's journey? Definition, stages, and examples](https://www.zendesk.com/blog/buyer-journey/)
-   📝 [Zapier - Customer journey mapping 101 (with free templates)](https://zapier.com/blog/customer-journey-mapping/)

### Project Management

-   📝 [GitLab - How to use GitLab for Agile portfolio planning and project management](https://about.gitlab.com/blog/2020/11/11/gitlab-for-agile-portfolio-planning-project-management/)
-   📝 [Adobe - Project scope - definition, best practices, examples, and more](https://business.adobe.com/blog/basics/project-scope-definition-best-practices-examples-and-more)

### Marketing Technology Audit

-   📝 [Blueconic - The MarTech stack evaluation guide](https://www.blueconic.com/resources/martech-stack)
-   📝 [Marketing Rockstar Guide - How to think about your Marketing tech stack](https://www.marketingrockstarguides.com/how-to-manage-martech-stack-3915/)

## 💼 Career Development

-   📝 [MarketingOps - 5 Steps to Take Your Junior Marketing Ops Team from Zero to MCE](https://marketingops.com/5-steps-to-take-your-junior-marketing-ops-team-from-zero-to-mce/)
-   🎙️ [OpsCast - How to continuously learn and grow in Marketing Ops](https://open.spotify.com/episode/5gVVWM5v54DASY7ZuPbPYw?si=7QjfC8KRQwmmwnrt6PsokA) 
-   📝 [LXA - Five tips to skyrocket your Marketing Ops career](https://www.lxahub.com/stories/5-tips-to-skyrocket-your-career-in-marketing-ops)

### Marketo

-   🎥 [Adobe - Advance Your Career: Adobe Marketo Engage Certifications](https://business.adobe.com/summit/2023/sessions/advance-your-career-adobe-marketo-engage-certifica-s203.html)
- 🎥 [Adobe - What $2 & a Dream Will Get You: Pushing Your Career Forward](https://business.adobe.com/summit/2023/sessions/what-2-a-dream-will-get-you-pushing-your-career-fo-s215.html)