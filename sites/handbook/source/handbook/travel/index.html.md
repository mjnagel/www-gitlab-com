---
layout: handbook-page-toc
title: Travel
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Expenses While Traveling

1. The company will cover all work-related travel expenses. This includes lodging and meals during the part of the trip that is work-related. Depending on the distance of your travel, it can also include one day before and one day after the work related business. For example, if you are attending a 3 day conference in a jetlag-inducing location, the company will cover your lodging and meals those 3 days as well as one day before and one day after.
1. The company will cover costs related to transiting to and from a destination or airport which is work-related. This includes public transit, ride-sharing services (Lyft, Uber, etc.), or personal vehicle mileage between one's home and airport, bus/rail station, or work-related meeting.
1. The company can accommodate custom requests. It is OK to stay longer on your trip. However, the extra days will not be covered by the company.
1. Understand the guidelines of our [Expense Policy](https://about.gitlab.com/handbook/finance/expenses/) before you travel.
1. Always bring a credit card with you when traveling for company business if you have one.
1. Hotels will generally expect you to have a physical credit card to present upon check-in. This credit card will be kept on file for the duration of your stay. Even if your lodging was pre-paid by the company or by using a company credit card, the Hotel may still require a card to be on file for "incidentals".
1. If you incur any work-travel related expenses (on your personal card or a GitLab company card), please make sure to save the original receipt.
1. When your trip is complete, please file an expense report via Navan Expense or include the receipts on your next monthly invoice.
1. Team member should be responsible for any increases in the airfare ticket price related to personal reasons, except for sickness and family emergencies.
  


## Booking travel and lodging<a name="TripActions"></a>

Before booking any flights please ensure that you have the proper [visas](/handbook/people-group/visas/#arranging-a-visa-for-travel) in place.

When your accommodation booking is confirmed, you may want to communicate the dates and address with your manager.

### Setting up your Navan (formerly known as TripActions) account

Below is some more information to help you get set up with your [Navan](https://tripactions.com/) account.

1. Log into [Okta](https://gitlab.okta.com) and click the Navan tile.
1. Once you are in your dashboard, make sure to set up your info in your profile (see link from drop down menu under your name).
1. Add your passport information to your profile (only passport information is accepted), this will populate your info directly when booking a flight.
1. Now let's start booking!

### Booking travel through Navan <a name="booking-travel"></a>

**Flights**

All flights need to be booked through [Navan](https://tripactions.com) (Business related flights), the costs will be added to the GitLab invoice and **no credit card is needed**. Please note that all business related flights will not have the option to pay with a personal credit card. Also to note that while it is possible to find a cheaper flight outside of Navan, we get overall better rates in the aggregate with the buying power of the whole company. Flights should not exceed $1K USD (or local currency equivalent) per team member per month for economy class, with a booking lead time of 7 days to be within policy.  The exception is for the Sales teams, in which flights should not exceed $3K (or local currency equivalent) per team member per month.  Please note that anything booked outside of Navan will flag as “Out of Policy”, and will need to be reviewed by your manager and the AP team.  If booked within Navan and is flagged as “Out of Policy”, please also refer to the "Booking shows Out of Policy" section below.

Team members should book Economy class and they must obtain email pre-approval to fly any class other than economy.

**Hotels and Cars**

When you book Hotels and/or Cars through Navan Travel, the costs will need to be charged to a personal credit card. For business related bookings, you can submit the costs for reimbursement via the Navan Expense reimbursement tool.  Hotel stays are reimbursable up to $300 USD (or equivalent local currency) inclusive of tax per night for standard rooms, with a 7 day booking lead time.  Always choose the refundable room booking unless the only available option is a non refundable room.  The exception is the Corporate Events hotel room blocks organized by the internal team or if the conference hotel room block has better pricing than Navan. The hotel room block's pricing is generally withn the boundds of the $300USD/night and in the event it is over that amount, please contact the AP team for an exception to be granted. Conference hotel room blocks or Corporate Events organized blocks will still need to be charged to a personal credit card and then submitted for reimbursement via Navan Expense. Rental Cars are reimbursable for the intermediate car class, with a 7 day booking lead time.  Please note that anything booked outside of Navan will flag as “Out of Policy”, and will need to be reviewed by your manager and the AP team.  If booked within Navan and is flagged as “Out of Policy”, please also refer to the "Booking shows Out of Policy" section below.

**Additional Car Rental Guidelines**

A. Third Party Liability

Purchase the liability insurance that is excess of the standard inclusion of State minimum coverage in the rental agreement at the rental agency. GitLab’s insurance policy provides liability insurance for rental cars while conducting company business, but it may be excess over any underlying liability coverage through the driver or credit card company used to purchase the rental.

Purchase the liability offered at the rental counter if there are foreign employees renting autos in the US or Canada. While workers' compensation would protect an injured US employee, other passengers may have the right to sue. To ensure that GitLab has protection when a foreign employee invites another person into the car we recommend the purchase of this insurance when offered at the rental counter.

B. Physical Damage - Collision Damage Waiver

Do Not purchase the Collision Damage Waiver offered at the rental counter. GitLab purchases coverage for damage to rented vehicles. If travel to Mexico is required, purchase the liability insurance for Mexico offered at the rental counter. You should verify that the rental agreement clearly states that the vehicle may be driven into Mexico and liability coverage will apply.

Countries other than the US and Canada

Third Party Liability

Purchase the liability insurance offered at the rental counter when traveling outside the US and Canada. Automobile Bodily Injury and Property Damage Liability insurance are required by law in almost every country. Please verify this coverage is included with the rental agreement.

Physical Damage - Collision Damage Waiver

Purchase the Collision Damage Waiver or Physical Damage Coverage offered by the rental agency when traveling outside the US and Canada.

In the event of an accident resulting in damage to the rental car, the foreign rental agency will charge the credit card used to make the reservation with an estimated amount of repair costs if insurance is not purchased. If this happens, GitLab does not purchase Foreign Corporate Hired Auto Physical Damage Coverage to reimburse for damages.


**Additional Hotel Guidelines**

Dry cleaning is allowable if the stay is over 3 days. Room service is allowable as a meal for ex. breakfast/lunch/dinner. Meals are to be segregated from the hotel bill and tagged under “Meals Company Provided” Category. Parking must be segregated and tagged under “Transportation”. Movies, mini bars are not allowed as a claim.


**Trains**

When you book Trains through Navan, business related bookings should be, upon receipt of manager's approval, paid upfront and expensed through Navan Expense reimbursement tool. Train rides are reimbursable up to $100 USD (or equivalent local currency) per ride for standard cabin class, with a 7 day booking lead time.  Please note that anything booked outside of Navan will flag as “Out of Policy”, and will need to be reviewed by your manager and the AP team.  If booked within Navan and is flagged as “Out of Policy”, please also refer to the "Booking shows Out of Policy" section below.

**Personal Bookings**

In Navan, you have the ability to book Personal trips. Note that when selecting this option, a personal credit card will be required at time of booking. Personal trips will not have the option to be added to the GitLab invoice.

#### Basic economy in Navan

Many airlines offer [basic economy](https://en.wikipedia.org/wiki/Basic_economy) fares by default. This can create confusion and stress on the traveler if the conditions of their airline ticket aren't known.

To ensure a trouble-free experience with air travel booked through Navan:

- If basic economy conditions are unsuitable, [filter out basic economy fares](https://tripactions.com/blog/basic-economy-fares-in-tripactions) from search results.
- Ahead of time, using the value in the `CONFIRMATION` field in a TripActions trip entry, look up your flights with the airline's own website to ensure times and conditions are what you expect.
- Check in using the airline's website (not Navan) as soon as check-in becomes available. This will make it clear ahead of time if checked baggage must be added to a flight at additional cost, and provide time to contact the airline in case of any other issues.

#### How to book

1. Please note that some budget airlines might not show, so, if you want that, make sure to check those to be sure there is no better option out there. (SouthWest, JetBlue, etc.). With the Navan tool, you can book both your travel and submit your expenses with ease, as your trips will integrate with your expenses, making coding and submission faster.  Refer to the Navan End Users Guide for more details.
1. Login to your account. If you've not yet set up your account, do this [first](#setting-up-your-navan-formerly-known-as-tripactions-account).
1. The dashboard gives you the option to start a booking directly. The options are
to book flights, hotels, trains, or rental cars.
1. Input info for the flight you want to book. You can pick return as a combo or each leg separately
1. Select "continue to checkout" and fill out the reasons for booking.
1. Once confirming all details are correct you can select "complete booking".
1. If your trip is out of policy, you will have the ability to continue booking the trip and a "soft" approval email will be sent to your direct manager. Please refer to "Booking shows Out of Policy" below.
1. You can add a hotel or car by clicking the boxes between the flight details and "complete booking" and go through the same steps.

Note: Once a flight is booked, re-routing is at the person’s own cost unless requested by the company or force majeure (in the latter case often the airline will cover the difference).

#### Meals and Internet While Traveling

1. Meals while traveling are reimbursable for up to $100 USD (or local equivalent currency) per day.  Each team member must submit their own receipts for reimbursement, so ensure to separate billing when going to meals with groups of team members.
1. Internet while traveling is reimbursable for up to $50 USD (or local equivalent currency) per day.


### FAQ about travel

Please read through these FAQ **entirely**.

1. If you have trouble finding travel within a budget, contact **your manager** about this, _not_ Navan.
1. If your flight hasn't been approved, contact **your manager**, they are responsible, _not_ Navan.
1. If your flight does not have a seat selection or checked bags included, you need to arrange that with **the airline**. Usually done upon check-in online, 24H before your flight.
1. If part of your travel has been changed after you've booked, contact **Navan Support** about this, they can help.

#### FAQ about travel upgrades

1. If you exceed 6'5" in height, you may:
    - Expense the full cost for an upgrade to Economy Plus on any flight
    - Expense up to the first [$300 USD](https://www1.oanda.com/currency/converter/) for an upgrade to Business Class on flights longer than 8 hours
1. If your height does not match the exact cm/inch requirement, and you feel unsafe to travel, you may:
    - Expense up to the first [$100 USD](https://www1.oanda.com/currency/converter/) for an upgrade to Economy Plus on flights longer than 8 hours
    - Reach out to leaves@gitlab.com to request a reasonable accommodation.
1. GitLab does not cover expenses for significant others or family members for travel or immigration. This includes travel and visas for GitLab events.

#### Booking shows Out of Policy

- Sometimes there is no way around booking a flight or hotel that is out of policy.
- This means that the option selected is more expensive than the cheapest option + our buffer.
- When you try to book this, an email will be sent to your manager for approval.
- Please provide extensive reasoning to make approval easier and faster.
- After 24 hours without any manual action (approved/denied) the booking will automatically be accepted.

#### Booking accommodations through Airbnb

- To be booked on AIRBNB platform. Same restrictions in amount applies as the hotel expense.
- Team member to submit a manual expense in Navan using expense type "Lodging" and attach the receipt/invoice.

#### Supporting nursing mothers

If you are nursing while traveling on GitLab approved business, you can reimburse costs in order to travel with breast milk or ship your breast milk back home. You can expense:

- Fees associated with a milk shipment service, such as [MilkStork](https://www.milkstork.com/)
- If a milk shipment service is unavailable, you may expense the cost of checking a bag while flying
- Coolers or containers to store breast milk while traveling
- Shipment costs and materials, including dry ice, packaging, styrofoam coolers, and labels. 

Check with your health plan to see if you are eligible for reimbursement for the cost of a breast pump or any other supplies. 

## Self Stay Incentive Policy <a name="self-stay"></a>

If you decide to stay with friends or family instead of a hotel / Airbnb for a business trip, the company will pay you 50% of what would have been
the cost of a reasonable hotel. A reasonable hotel would be the negotiated hotel rate a conference provides. This is in keeping with our values of frugality and freedom, and intended to reward you for saving company money on accommodation expenses. Contractors should take a screenshot of a hotel search from the same location in TripActions and submit this along with your expense report or invoice. Team Members Employed through a GitLab Entity should submit the screenshot for approval via [Navan Expense](https://about.gitlab.com/handbook/finance/expenses/#-introduction).

## Travel Safety and Security Policy

Team member safety and security are a top priority for GitLab. If a team member feels uncomfortable or unsafe traveling to a location on GitLab's behalf due to concerns about the location, region, personal health, or other safety or security risks, please contact [People Connect](https://gitlab.slack.com/archives/C02360SQQFR) via Slack or email. The People Connect team will assist by helping to connect the team member with the right resources.

** Please note that team member travel is not required unless deemed essential to the role. Please speak to your manager about whether the travel is mandatory and required for your role.

## Travel Insurance

GitLab offers travel insurance for business travel. Full details of GitLab's Business Travel Accident Policy can be found in the [benefits section](/handbook/total-rewards/benefits/general-and-entity-benefits/#business-travel-accident-policy).

## Emergency Travel Assistance While Traveling Abroad for Work

Emergency Medical, Safety, and Security Assistance is available to all GitLab team members when traveling abroad for work.  All GitLab team members can access AIG's Travel Guard program for emergency assistance while traveling. Prior to traveling:
- Download the [Travel Guard App](https://apps.apple.com/app/aig-business-travel-assistance/id829194220) and register for their services.  If you don't have access to the app and need assistance, contact the following emergency call centers 24x7:
- View and register on [Travel Guards](https://travelguard.secure.force.com/TravelAssistance/TGPreLoginHomePage?PL=AIGACCIDENTANDHEALTH) website for resource information
- Download and save the [GitLab Assistance Membership card](https://drive.google.com/file/d/1WJUmO1AC2gIwgBuA8FgacgUwJuWdlutd/view?usp=sharing) and keep it handy for emergency contact details.
- Emergency Phone Contact Information:
- U.S. and Canada Toll-Free: 1-877-244-6871 or +1-877-278-7196
- Non-U.S. collect calls:  +1 715-346-0859 or +1 715-295-9973

## Tips on Working Remotely While Abroad

Working remotely while being abroad can be quite different. For example it can be harder to arrange good working internet and you may need extra tools to have a similar level of comfort to be able to work effectively.

Learn more about [optimizing comfort and efficiency when taking your office with you while traveling](/company/culture/all-remote/working-while-traveling/).

### Flights

Planning flights far in advance can help you get cheaper fares when booking. For example you can set alerts when there is a significant drop in price for your travel destination.

- [Google Flights](https://www.google.com/flights/) - Great for comparing and setting up alerts
- [Skyscanner](https://www.skyscanner.com) - Great for comparing and setting up alerts
- [Kiwi](https://www.kiwi.com/) - Great experience for booking flights on mobile devices
- [Airwander](http://airwander.com/) - Great for booking flights with stopovers

What if you find a cheaper flight outside TripActions? If you are able to find a better price elsewhere, feel free to contact `support@tripactions.com` or call `+1-(888)-505-TRIP (8747)` for help. Please note, TripActions cannot match flight pricing found on “discount websites” or “flash sales” that are sometimes advertised as part of an airline sale. They are bound to the prices set by the airlines.

### Navigation

Offline navigation content is key when you are not certain of an internet connection.

- [Maps.me](http://maps.me/en/home) - Large offline maps, lots of destination content
- [Google Maps](https://www.google.com/maps) - Google maps allows offline downloads for self selectable areas

### Communication

When you have an internet connection, it can save you quite a bit of money to be able to communicate just using data.

- [WhatsApp](https://www.whatsapp.com/) - Most used app, which supports chat and video/voice calling
- [Telegram](https://telegram.org/) - Great for places where WhatsApp isn't working
- [Signal](https://signal.org/) - Alternative to WhatsApp with more emphasis on privacy and security
- [Slack](https://slack.com/)
- [Google Duo](https://duo.google.com/) - Great for video and/or voice calling, especially on low bandwidth connections

### Special Shoutouts

Applications which are all around wonderful to have when traveling.

- [Google Now](http://www.google.nl/landing/now/) - an intelligent personal assistant developed by Google. Learn more about [Google Now on Wikipedia](https://en.wikipedia.org/wiki/Google_Now).
- [Google Trips](https://get.google.com/trips/) - Makes planning your day very easy with suggestions for things to see and do

### Handling Currencies

The following tools make paying someone back in their own currency a lot easier.

- [PayPal](https://www.paypal.com)
- [Wise](https://wise.com/) (Formerly TransferWise)
- [Revolut](https://www.revolut.com/)

### Splitting Costs

Traveling together or letting someone else pay something for you? This makes it easier to see how costs add up or should be split up.

- [Splitwise](https://www.splitwise.com/) - Supports a great amount of currencies and is well designed

### Internet Problems and VPNs

- In China most services we use daily are not working due to [the great firewall of China](https://en.wikipedia.org/wiki/Great_Firewall). WhatsApp seems to work though!

VPNs can help you reach services or sites that are blocked, get contents that are not available abroad or let you browse the internet as if you were home (with your own language preselected).

- [ZenMate](https://zenmate.com/) - Great application with a good 7-day free trial
- [Opera](http://www.opera.com/) - Has a built in free VPN

_Note that a VPN may not always work as intended. For example both Netflix and China have blocked certain VPNs, limiting their usefulness._

### Media and Entertainment

The following services provide offline functionality for their content if you have a paid membership.

- [Spotify](https://www.spotify.com)
- [Soundcloud](https://soundcloud.com)
- [Netflix](https://netflix.com)

### Power

Electricity is a requirement on your journey abroad. Be it to power your phone or your laptop.

- [Anker powerbanks](https://www.anker.com/) - Great value for the price, including USB-C options
- [Skross world adapter pro](http://www.skross.com/en/product/87/world-adapter-pro.html) - All around great travel adapter, usable in almost every country

### Secure your data during travels

During your working travel your restricted data could be exposed.
If you feel that your travel frequency may expose your data please keep in mind the following points to ensure that sensitive data contained in your devices will not be compromised:

- VPN - If you are connecting from an untrusted network you should use a VPN connection to avoid [MITM Attack](https://en.wikipedia.org/wiki/Man-in-the-middle_attack) or similar.
- Devices in public places - If you are using your device in a public place someone could read restricted data from your screen, you should protect your screen with a special film that ensure your privacy, [here you can find some samples](https://www.amazon.com/s?k=privacy+screen+filter)
- Speaking in crowded places - Ensure that when you are talking about restricted data you are in a secure place and no-one can hear you.
- 1Password travel mode - If you are approaching travel in a risky country or you have to leave your devices in an insecure place, please use [Travel Mode](https://support.1password.com/travel-mode/) in 1Password to ensure that your vaults will be safe if your device is compromised.

When traveling, there is the possibility that customs, border control, or other law enforcement agents demand that you provide the password to your laptop or mobile device. If this happens to you in regards to a company-owned account or device, GitLab recommends that you comply with any such requests without protesting. If you do end up providing a password to any device or account that has access to GitLab resources (including your personal devices), you should follow [the process to engage Security on-call](/handbook/security/security-operations/sirt/engaging-security-on-call.html) as soon as you can safely do so. Even if you do not provide a password, if any of your devices are permanently seized or even temporarily removed from sight, you should also engage the Security on-call.

For additional security specifically when traveling through an airport, the following advice is provided:

- Completely power-off laptops and mobile devices before any security checkpoints. This reduces the risk of attacks that may be conducted against machines in a sleep mode, where the disk encryption is not being actively enforced.
- Prior to the trip, remove any sensitive data from your local machine that is not absolutely required. Refer to the [Data Classification Standards](https://about.gitlab.com/handbook/security/data-classification-standard.html#data-classification-standards) for more information.
- If you are travelling through an [embargoed country](https://about.gitlab.com/handbook/sales/#export-control-classification-and-countries-we-do-not-do-business-in), do not bring any company-owned devices with you. If you must bring one, contact the legal department to discuss.
- If you are aware of any circumstances with your travel that may present a unique security risk (such as traveling to speciality conferences like Defcon), you may request advice in the `#security-department` Slack channel.

## Travel Guidance: COVID-19

Our top priority is the health and safety of our team members.

### Covid-19 Travel Working Group

The COVID-19 Travel Working Group is on hold and will be re-activited if Covid-19 or another significant public health risk arises.  If you have a question or feedback, please post in the slack channel:  #covid19-response-committee.  

### Policy and Guidelines

Where can GitLab Team Members Travel? While some areas of the world have decreased cases of Covid-19, some are increasing. Team Members must refer to the [CDC website](https://covid.cdc.gov/covid-data-tracker/#global-counts-rates)) to ensure that both locations (traveling from and to) are safe locations in which to travel.

### Required of/by Team Members Prior to Traveling:

This section has been updated as of 2023-05-01

Due to the consistent decline in Covid-19 cases and hospitalizations, GitLab continues to highly recommend but will no longer require the Covid-19 vaccination for team members who need to travel or attend in-person gatherings.  However, we are still asking team members who gather to follow our Covid-19 testing requirements.  Continuing our testing program helps protect team members and their families at higher risk of Covid-19.  These requirements include that team members test for Covid-19 three days before in-person gatherings and each morning before getting together while in attendance.  
 
All business travel still requires pre-approval by your manager per the expense policy.  Business travel requests should be made for any mode of travel (including personal vehicles for local travel and local public transportation) where team members will be meeting co-workers, customers and/or partners in person.

All international travelers must follow each country's guidelines governing entry and re-entry. If Covid-19 testing is required to travel, GitLab will reimburse team members for required testing costs.

### For In-Person GitLab Events:

If a meeting/meet-up includes 5 or more GitLab Team Members meeting in person, please follow these protocols:

### Covid-19 Testing:  
All GitLab attendees must test negative for COVID-19, three days prior to travel, and again each day of the event prior to meeting with team members.  

The cost of the test kits are often covered by medical insurance.  In the event that the tests aren't covered by your insurance plan, you may expense the cost of the Antigen tests required during business travel. If you are attending a large event, please check with your event facilitator who may have tests available at discounted/bulk prices.  

### Team Member Not Feeling Well

If a team member is not feeling well, regardeless of COVID-19 test results, team members are asked not to attend in person events or activities. Team members should seek medical advise from their healthcare provider to determine when it is safe to be around others. 

### Positive Test Result:  
If a Team Member tests positive for Covid-19, they are to refrain from meeting with others and must contact the facilitator of the meeting, and/or the venue contact, and notify teammemberrelations@gitlab.com via email. The team member may be required to quarantine until testing negative for Covid-19 which could take 7-10+ days.  Quarantine and isolation rules differ by country and that's where Team Member Relations can help determine next steps.  

If the team member is required to quarantine based on their location, and the team member is local to the event and traveled to the event by car, they may return and quarantine from home.  If they are not local and traveled to the event via plane or other public transportation, they will be required to follow applicable laws regarding quarantining, which may include being required to quarantine at the venue or location and may not be allowed to return home until testing negative.  For additional context and information about quarantining and isolating, please see the CDC Website [Quarantine and Isolating](https://www.cdc.gov/coronavirus/2019-ncov/your-health/quarantine-isolation.html.)   

### Notification of Positive Test Result:  
If you are the facilitator or coordinator of an in-person event, and if a participant at the event tests positive for Covid-19, please notify teammemberrelations@gitlab.com via email.  The transmission period runs from 2 days before the person felt sick (or, if not showing symptoms, 2 days before testing) until the time the person is isolated.  Please provide the name of the event, the dates, the location, and attendees.  We prefer for the team member who tested positive to report the test result to teammemberrelations@gitlab.com directly. If you are aware of someone who has tested positive, you too can report that you aware of a positive test to teammemberrelations@gitlab.com.  We will work to determine attendees and notification requirements based on local regulations, and to ensure that team members are alerted to the potential exposure to Covid-19.  However, if multiple positive Covid-19 cases are detected at the same event, the notification may be limited to the first occurrence only.  You should take any precautions you feel comfortable taking (for example, continue testing after the event for several days, isolate, watch for symptoms and if symptoms develop, follow your doctors orders, etc.)

General recommendations/precautions:

- Wash your hands frequently with soap and water or use an alcohol-based hand rub if your hands are not visibly dirty
- Practice good respiratory hygiene, that is, when coughing and sneezing, cover your mouth and nose with flexed elbow or tissue – discard tissue immediately into a closed bin and clean your hands with alcohol-based hand rub or soap and water
- Maintain physical distancing, that is, leave at least 2 metres (6 feet) distance between yourself and other people, particularly those who are coughing, sneezing and have a fever
- Avoid touching your eyes, nose and mouth – if you touch your eyes, nose or mouth with your contaminated hands, you can transfer the virus from the surface to yourself
- Keep the surfaces around you clean

### Personal Travel

You must make your own decision about what is right for you regarding travel. Please be aware that any risk of potential exposure to Covid-19 may subject you to potential restrictions from GitLab or third parties (ex. customers, vendors, conferences, etc.) on your ability to attend in-person meetings or events for some period of time, even after the removal of general travel restrictions. Consult authoritative agencies at both departure and arrival locations before embarking on international travel. If you choose to travel internationally, please note the resources provided by agencies such as the CDC (in the USA) and your local government, and we ask that you notify your manager with your travel dates so we can help set expectations for timing to return to possible in-person meetings in the future. Always consider ways to get the business results remotely before requesting travel approval.

### Resources:

- [World Health Organization Guidance](https://www.who.int/emergencies/diseases/novel-coronavirus-2019)
- [CDC Guidance](https://www.cdc.gov/coronavirus/2019-ncov/travelers/index.html)
- [Government of Canada Guidance](https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html)
- TripActions Application found via Okta - TripActions has information regarding [Travel Restrictions](https://app.tripactions.com/app/user2/travel-restrictions)

## Sustainable travel considerations

Even as an all-remote company, GitLab recognizes that some travel is necessary. These considerations aim to reduce the environmental impact of travel:

- Prefer virtual meetings and attendance over personal meetings, except when the event's objective is enhanced by meeting in real life.
- For short-distance travel, consider ground travel over air travel for information on short distance travel recommendations visit these examples from the [University of Bern](https://www.klimaneutral.unibe.ch/mobility/business_trips/travel_policy/index_eng.html), [University of Lund](https://www.lucsus.lu.se/sites/lucsus.lu.se/files/lucsus_travel_policy.pdf).
- Select trips with lower carbon emissions. When [booking travel though Tripactions](https://about.gitlab.com/handbook/travel/#booking-travel-through-tripactions-) you can view the total CO2 footprint for at the checkout screen. 
- If you organize an in-person meeting, aim for a location that allows as many people as possible to travel in a sustainable way.
- Carbon offsets [offered by airlines have dubious value](https://www.nationalgeographic.com/travel/article/should-you-buy-carbon-offsets-for-your-air-travel), so aim to avoid air travel where it makes sense.
