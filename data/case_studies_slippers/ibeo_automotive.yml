title: Ibeo Automotive Systems
cover_image: /images/blogimages/cover_image_ibeo_auto.jpg
cover_title: |
  GitLab places Ibeo Automotive Systems in pole position for driving innovation and scaling efficiencies
cover_description: |
  The German LiDAR sensor maker adopted GitLab for a modern software delivery platform
canonical_path: /customers/ibeo_automotive/
customer_logo: /images/case_study_logos/IBEO_LOGO.jpg
customer_logo_css_class: brand-logo-tall
customer_industry: Automotive
customer_location: Germany, USA & Holland
customer_solution: GitLab Self Managed Premium
customer_employees: 400+
customer_overview: |
  A customer since 2017, Ibeo moved up to GitLab Premium for increased merge velocity and enhanced collaboration.
customer_challenge: |
  Ibeo wanted a Git-centric DevOps practice to save time, make compliance easy, and keep developers happy.

key_benefits: >-


    Merge trains for simultaneous merges  


    MRs closed faster


    Less developer downtime


    A very stable master


    Improved collaboration


    Scalability


    Jenkins and Jira integration


    Happier developers
 

customer_stats:
  - stat: 2x 
    label: CI/CD sped up pipelines, saving the equivalent of two full-time employees’ work
  - stat: 20x    
    label: More merge requests put through in a day
  - stat: 4.38
    label: Out of 5 stars approval rating from devs (>87% approval rating)
customer_study_content:
  - title: the customer
    subtitle: A German supplier of LiDAR sensors
    content: >-
    

        Ibeo Automotive Systems GmbH is a 22-year-old automotive technology supplier with offices in Hamburg, Germany, Detroit, USA and Eindhoven, Holland. The company specializes in light detection and ranging (LIDAR) sensors used primarily in car safety systems and also for autonomous driving. “Ibeo's goal is to reinvent mobility and for every customer to become a partner in the process,” said Till Steinbach, Product Manager for Electronic Control Units and Software Integration at [IBEO](https://www.ibeo-as.com/en). “We want to make transportation safer, more reliable, and maybe also more fun.”

  - title: the challenge
    subtitle:  Start from scratch with Git and DevOps
    content: >-
    

        Suppliers to the European auto industry are heavily regulated, and as the company grew from selling to suppliers to also selling to original equipment manufacturers the scrutiny increased. “Our sensors are part of the decision process of a machine and there’s no room to make mistakes,” Steinbach explained. In addition, they needed a comprehensive solution to support all of these processes. 
    

        Ibeo decided to completely rethink how software was developed. Steinbach said the company wanted a Git-centered DevOps practice that could support all the compliance requirements and be easier on the development team. “We really wanted to provide an environment for our developers that is modern and also enjoyable,” he said.
    

        His concern about developer job satisfaction was real: some years ago, Ibeo was using Subversion, didn’t have any kind of a CI/CD strategy, and was heavily reliant on manual processes for nearly everything. “Manual processes take a lot of time, so you’re wasting a lot of time,” he said. “It’s not very reproducible and it’s not much fun. Developers are usually keen to create new things, and this is like an overhead or a burden they have.”
    

        Ibeo had a list of “must-haves” when it came to their solution: The platform had to be open source, self-managed, scalable, capable of integrating with Jenkins and custom tools, provide for truly modern software development, and be attractive to the local workforce. Because Ibeo is in the more “traditional” automotive space and located in Hamburg (not always considered a hub of automotive software development), the company was more likely to hire graduates from local universities, so having a cutting-edge development solution was required to attract the brightest workforce.
    

        Beyond merely meeting requirements, Ibeo wanted to unlock a new stream of innovation, which required a fundamental change in tooling. The company’s prior pipeline could only handle 20 merge requests in a 24-hour period and merge requests were often open for a week. And in a regulated industry, “We need to do a lot of checking for our product to be really sure that what is going into our master branch is okay and that it’s safe,” Steinbach said, so better and more integrated code reviews were key.
 
 
  - title: the solution
    subtitle:  From Starter to Premium for CI/CD and merge trains
    content: >-
    

        In 2017, Ibeo moved from SVN to GitLab. With fewer than 70 employees, Ibeo chose GitLab Starter and Steinbach said the company immediately saw improvements. Ibeo moved from many small repositories to a single unified one and, as time passed, watched how scalable GitLab was as the company grew to over 400 people. Now on GitLab Premium, Ibeo uses a Jenkins pipeline (so it doesn’t lose access to custom tooling) and a custom build tool, and both are well integrated into GitLab. Ibeo doesn’t use Docker or the cloud at any stage of deployment in part because of the unique nature of what they’re making: “We’re building an embedded product,” Steinbach explained. “This is code that is uploaded on hardware that is driving around” so there’s no benefit to a cloud solution, at least right now.
    

        Ibeo uses Jira for project management across the entire company, Steinbach said, so it’s not surprising the development team uses it as well to manage merge requests. Because Jira is well integrated with GitLab, the process has been seamless, though he said this is an area the team is still exploring.


  - title: the results
    subtitle: More MRs, less time wasted, and very happy developers
    content: >-
    

        It’s safe to say the Ibeo development team has seen sweeping changes as a result of moving to GitLab Premium. Developers can merge in parallel (thanks to merge trains), meaning no more limits on numbers of merge requests and no more MRs open for long periods of time. “Just by going to Premium and enabling the merge train feature we saved the equivalent of two full-time employees,” Steinbach said. “The time that developers used to spend on waiting and checking merge requests is now time they spend on the product. That made this upgrade totally reasonable.”
    

        All told Ibeo is now able to merge between 200 and 250 requests per day, making their development team’s efforts far more scalable. And open MR time has decreased as well, from an average of three days before GitLab Premium to an average of five hours now. “This is time saved for the developers, but this makes us also faster in releasing and faster in fixing things,” Steinbach said.
    

        Steinbach admitted he was surprised that with the increased developer activity and the GitLab/Jenkins integration for CI/CD that the master has been so stable. The master’s only been broken “very few times,” he said. “I’m pretty amazed that just by introducing tooling we were able to get to such a stable situation.”
    

        “Collaboration at Ibeo has also improved thanks to GitLab. Not only is the team not having to spend time dealing with disparate tools, but collaboration is easier. “We are using one repository, not only together with several teams, but even together with several business units,” Steinbach said. There are a lot of changes that impact a lot of teams and GitLab really makes it easy for us to collaborate and get approval from all the teams that a change is accepted.” 
    

        And finally, developers at Ibeo really are happier thanks to GitLab, Steinbach said. The company did a “satisfaction” survey about GitLab and it rated 4.38 stars out of five. “Developers like the approval routes for code review, the merge request dependencies...they like pretty much everything,” Steinbach added.
    
  
        ## Learn more about GitLab solutions
    

        [Value Stream Management with GitLab](/solutions/value-stream-management/)
    

        [CI with GitLab](/features/continuous-integration/)
    

        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: GitLab helps us overcome the problems we had and it allows us to enforce our processes and prevent developers from making mistakes in a very gentle way.
    attribution: Till Steinbach 
    attribution_title: Product Manager for Electronic Control Units and Software Integration at Ibeo

 



