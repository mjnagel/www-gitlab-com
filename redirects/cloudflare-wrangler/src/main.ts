import { Redirects } from './redirects';
import redirectsYaml from '../../../data/redirects.yml';
import yaml from 'js-yaml';

declare const DEBUG: boolean;

export interface Env {
  REDIRECTS: KVNamespace;
  ENVIRONMENT: string;
  BUCKET: string;
  DEFAULT_CACHE_TTL: number;
}

const DEFAULT_CACHE_TTL = 60;

export default {
  async fetch(clientRequest: Request, env: Env, ctx: ExecutionContext): Promise<Response> {
    const clientRequestURL = new URL(clientRequest.url);
    const { pathname, search, hostname } = clientRequestURL;

    // Redirect/strip leading "www."
    if (hostname.startsWith('www.')) {
      return Response.redirect(`https://${hostname.slice(4)}${pathname}${search}`, 301);
    }

    // If URL ends with `/index.html` then redirect to the same path minus the `/index.html`
    if (pathname.endsWith('/index.html')) {
      const pathWithoutIndexHtml = pathname.replace(/\/index.html$/, '/');
      return Response.redirect(`https://${hostname}${pathWithoutIndexHtml}${search}`, 301);
    }

    // Redirect requests from China with a path that starts
    // with /install to about.gitlab.cn
    if (clientRequest.cf?.country === 'CN' && pathname.startsWith('/install')) {
      return Response.redirect(`https://about.gitlab.cn${pathname}${search}`, 301);
    }

    let redirects;
    let basePath;

    if (env.ENVIRONMENT === 'review') {
      // Prefix path with subdomain for review app
      const subdomain = hostname.split('.', 1)[0];
      const originPathPrefix = `/${env.BUCKET}/${subdomain}`;
      basePath = originPathPrefix;

      // Check if we have a redirect key for this MR
      if (env.REDIRECTS) {
        const mrRedirects = await env.REDIRECTS.get(subdomain);

        if (mrRedirects !== null) {
          if (DEBUG) {
            console.debug(`Using redirects from KV key '${subdomain}' for ${clientRequest.url}...`);
          }

          redirects = new Redirects(yaml.load(mrRedirects));
        }
      }

      let path = pathname;
      if (path.endsWith('/')) {
        // eg: / => /index.html
        // eg: /get-started/ => /get-started/index.html
        path += 'index.html';
      } else if (/^(.*)\/([^.]+)$/.test(path)) {
        // eg: /search => /search/index.html
        path += '/index.html';
      }

      clientRequestURL.pathname = `${originPathPrefix}${path}`;
    }

    if (!redirects) {
      if (DEBUG) {
        console.debug(`Using redirects from the worker for ${clientRequest.url}...`);
      }
      redirects = new Redirects(yaml.load(redirectsYaml));
    }

    let redirect = redirects.getExactMatch(pathname, `https://${hostname}`);
    if (redirect) {
      if (DEBUG) {
        console.debug(`FOUND - exact match redirect for '${pathname}' => redirecting to: ${redirect}`);
      }

      return Response.redirect(redirect, 301);
    }

    redirect = redirects.getStartsWithMatch(pathname, hostname, search, `https://${hostname}`);
    if (redirect) {
      if (DEBUG) {
        console.debug(`FOUND - 'starts with' regex match for '${pathname}' => redirecting to: ${redirect}`);
      }
      return Response.redirect(redirect, 301);
    }

    redirect = redirects.getAppendMatch(pathname, hostname, search, `https://${hostname}`);
    if (redirect) {
      if (DEBUG) {
        console.debug(`FOUND - 'append' regex match for '${pathname}' => redirecting to: ${redirect}`);
      }
      return Response.redirect(redirect, 301);
    }

    // Add trailing slash if needed (if there is no query string)
    if (search === '' && /\/[^./]+$/.test(pathname)) {
      if (DEBUG) {
        console.debug(`Missing trailing slash => redirecting to: ${clientRequest.url}/`);
      }
      return Response.redirect(`${clientRequest.url}/`, 301);
    }

    // Fetch from the origin
    const beResponse = await fetch(clientRequestURL, {
      cf: {
        cacheKey: `https://${hostname}${pathname}${search}`,
        cacheEverything: true,
        cacheTtlByStatus: {
          '200-299': env.DEFAULT_CACHE_TTL || DEFAULT_CACHE_TTL,
          404: 1,
          '500-599': 0,
        },
      },
      redirect: 'manual',
    });

    if (beResponse.status === 404 && env.ENVIRONMENT === 'review') {
      clientRequestURL.pathname = `${basePath}/404.html`;
      if (DEBUG) {
        console.debug(`Fetching 404 page: ${clientRequestURL.toString()}`);
      }

      const fetchNotFound = await fetch(clientRequestURL);

      return new Response(fetchNotFound.body, {
        status: beResponse.status,
        statusText: beResponse.statusText,
        headers: fetchNotFound.headers,
      });
    } else if (beResponse.status >= 300 && beResponse.status <= 399) {
      if (DEBUG) {
        console.debug(`Received ${beResponse.status} from the origin. Redirecting user to: ${beResponse.headers.get('location')}`);
      }

      return beResponse;
    }

    // Reconstruct the Response object to make its headers mutable
    const clientResponse = new Response(beResponse.body, beResponse);

    // CORS control
    if (/^https?:\/\/about\.gitlab\.com$/.test(clientRequestURL.origin)) {
      clientResponse.headers.set('Access-Control-Allow-Origin', clientRequestURL.origin);
    }

    clientResponse.headers.append('Vary', 'Origin');

    return clientResponse;
  },
};
